from api_favorites import create_app

# * Init app
app = create_app()

# * Run Server
if __name__ == '__main__':
    app.run(port=app.config['PORT'])
