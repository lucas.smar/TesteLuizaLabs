from api_favorites import db, ma

# * ExternalApp Model


class ExternalApp(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    app_name = db.Column(db.String(100), unique=True)
    admin = db.Column(db.Boolean)
    access_key_hashed = db.Column(db.String(100))

    def __init__(self,  app_name, admin, access_key_hashed):
        self.app_name = app_name
        self.admin = admin
        self.access_key_hashed = access_key_hashed

# * ExternalApp Schema


class ExternalAppSchema(ma.Schema):
    class Meta:
        fields = ('app_name', 'admin', 'access_key_hashed')


external_app_schema = ExternalAppSchema()
