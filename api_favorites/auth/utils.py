from flask import current_app as app
from flask import request, jsonify
from functools import wraps
import random
import string
import jwt


def random_access_key(keyLength=10):
    key_characters = string.digits + string.punctuation+string.ascii_letters
    return ''.join(random.choice(key_characters) for i in range(keyLength))


def token_required(f):
    @wraps(f)
    def internalFunc(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        elif 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'error': 'Token not found!'}), 401  # Unauthorized
        token = token.replace('Bearer ', '')
        try:
            data = jwt.decode(token, app.config['JWT_SECRET_KEY'])
        except:
            return jsonify({'error': 'Token is not valid, get a new token.'}), 401

        return f(*args, **kwargs)

    return internalFunc
