from flask import current_app as app
from flask import request, Blueprint, jsonify
from api_favorites import db, argon2
from api_favorites.auth.model import ExternalApp, external_app_schema
from api_favorites.auth.utils import random_access_key, token_required
import jwt
import datetime

auth = Blueprint('auth', __name__)


# * Login
@auth.route('/login', methods=['POST'])
def check_external_app_credentials():
    app_name = request.json['app_name'] if 'app_name' in request.json else ''
    access_key = request.json['access_key'] if 'access_key' in request.json else ''
    external_app = ExternalApp.query.filter(
        ExternalApp.app_name == app_name).first()
    if external_app:
        the_app = external_app_schema.dump(external_app)
        if argon2.check_password_hash(the_app.get('access_key_hashed'), access_key):
            token = jwt.encode({'app_name': the_app.get('app_name'), 'admin': the_app.get('admin'), 'exp': datetime.datetime.utcnow(
            ) + datetime.timedelta(minutes=30)}, app.config['JWT_SECRET_KEY'])
            return jsonify({"token": token.decode('UTF-8')})
        else:
            return jsonify({"error": "The access_key or the app_name is not valid, please check and try again."})
    else:
        return jsonify({"error": "The access_key or the app_name is not valid, please check and try again."})

# * Create new app credentials
@auth.route('/newApp', methods=['POST'])
# @token_required # Just for testing propose
def create_new_app_credentials():
    app_name = request.json['app_name']
    exist = ExternalApp.query.filter_by(app_name=app_name).first()
    if exist:
        return jsonify({"error": "This app name is already taken."})

    access_key = random_access_key(50)
    admin = request.json['admin'] if 'admin' in request.json else False
    access_key_hashed = argon2.generate_password_hash(access_key)
    new_app = ExternalApp(app_name, admin, access_key_hashed)
    db.session.add(new_app)
    db.session.commit()
    return jsonify({"access_key": access_key, "message": "Please, keep this access key secure. It is your responsibility to remember it."})
