from api_favorites import db, ma

# * Client Model


class Client(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True)

    def __init__(self, id, name, email):
        self.id = id
        self.name = name
        self.email = email

# * Client Schema


class ClientSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'email')


client_schema = ClientSchema()
clients_schema = ClientSchema(many=True)
