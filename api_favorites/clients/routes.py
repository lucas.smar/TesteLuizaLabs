from flask import request, Blueprint, jsonify
from api_favorites import db
from api_favorites.clients.model import Client, client_schema, clients_schema
from api_favorites.auth.utils import token_required
import uuid


clients = Blueprint('clients', __name__)

# * Apply token verify for all routes in client blueprint
@clients.before_request
@token_required
def just_to_check_if_is_logged():
    pass

# * Create one Client
@clients.route('', methods=['POST'])
def create_client():
    name = request.json['name'] if 'name' in request.json else None
    email = request.json['email']if 'email' in request.json else None
    if not name or not email:
        return jsonify({"error": "Please send all required fields"})
    client = Client.query.filter_by(email=email).first()
    if client:
        return jsonify({"error": "This email is already taken."})
    id = str(uuid.uuid4())

    new_client = Client(id, name, email)

    db.session.add(new_client)
    db.session.commit()
    return client_schema.jsonify(new_client)

# * Get All Clients
@clients.route('', methods=['GET'])
def get_all_clients():
    all_clients = Client.query.all()
    result = clients_schema.dump(all_clients)
    return jsonify(result)

# * Get one Client
@clients.route('/<id>', methods=['GET'])
def get_one_client(id):
    client = Client.query.get(id)
    return client_schema.jsonify(client)

# * Edit one Client
@clients.route('/<id>', methods=['PUT', 'PATCH'])
def edit_client(id):
    client = Client.query.get(id)
    client.name = request.json['name'] if 'name' in request.json else client.name
    email = request.json['email'] if 'email' in request.json else client.email
    if email:
        if email != client.email:
            exist = Client.query.filter_by(email=email).first()
            if exist:
                return jsonify({"error": "The new client email is already taken."})
    client.email = email
    db.session.commit()
    return client_schema.jsonify(client)

# * Delete one Client
@clients.route('/<id>', methods=['DELETE'])
def remove_one_client(id):
    client = Client.query.get(id)
    db.session.delete(client)
    db.session.commit()
    return client_schema.jsonify(client)
