from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from api_favorites.config import Config
from flask_argon2 import Argon2

db = SQLAlchemy()
ma = Marshmallow()
argon2 = Argon2()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)
    ma.init_app(app)
    argon2.init_app(app)

    from api_favorites.clients.routes import clients
    app.register_blueprint(clients, url_prefix='/client')

    from api_favorites.favorite_list.routes import favorite_list
    app.register_blueprint(favorite_list, url_prefix='/list')

    from api_favorites.auth.routes import auth
    app.register_blueprint(auth, url_prefix='/auth')

    from api_favorites.cache_products_api.routes import cache
    app.register_blueprint(cache, url_prefix='/cache')

    return app
