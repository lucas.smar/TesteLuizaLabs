from flask import current_app as app
from flask import request, Blueprint, jsonify
from api_favorites.auth.utils import token_required
from api_favorites.cache_products_api.utils import getAllPages, clearRedis, removeOne, getProductFromAPI, setProduct
import datetime

cache = Blueprint('cache', __name__)

# * Get all products from external api and include in redis
@cache.route('/seed', methods=['POST'])
@token_required
def seed_cache():
    requestBody = request.get_json(silent=True)
    if requestBody:
        if 'ttl' in requestBody:
            ttl = requestBody['ttl']
        else:
            ttl = app.config['REDIS_TTL']
    else:
        ttl = app.config['REDIS_TTL']
    return getAllPages(ttl)

# * Clean redis
@cache.route('/cleanAll', methods=['POST', 'DELETE'])
@token_required
def clean_cache():
    clearRedis()
    return jsonify({"message": "Cache cleared!"})

# * Add or update a product in redis
@cache.route('/addOrUpdate/product/<product_id>', methods=['POST', 'PUT', 'PATCH'])
@token_required
def include_one_product(product_id):
    requestBody = request.get_json(silent=True)
    if requestBody:
        if 'ttl' in requestBody:
            ttl = requestBody['ttl']
        else:
            ttl = app.config['REDIS_TTL']
    else:
        ttl = app.config['REDIS_TTL']
    result = getProductFromAPI(product_id)
    if result:
        setProduct(result, ttl)
        return jsonify({"message": "Product updated or add in cache!"})
    return jsonify({"message": "Product do not exist in products api!"})

# * Remove one product from redis
@cache.route('/remove/product/<product_id>', methods=['DELETE'])
@token_required
def clean_one_key(product_id):
    result = removeOne(product_id)
    if result == 1:
        return jsonify({"message": "Product removed from cache!"})
    else:
        return jsonify({"message": "Product do not exist in cache!"})
