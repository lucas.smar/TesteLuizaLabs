from flask import current_app as app
import requests
import redis
import json
redis = redis.Redis()

# * Get one product
# * Check if the product exists in redis if not check if exists in product api
# * If exists in product api and do not exists in redis, include in redis


def getProduct(product_id):
    product = redis.hgetall(product_id)
    if product:
        product = {key.decode('UTF-8'): product.get(key).decode('UTF-8')
                   for key in product.keys()}
        product['id'] = product_id
        product['price'] = float(product['price'])
        if 'reviewScore' in product:
            product['reviewScore'] = float(product['reviewScore'])
        return product
    else:
        product = getProductFromAPI(product_id)
        if product:
            setProduct(product, app.config['REDIS_TTL'])
            product['id'] = product_id
        return product

# * Get one product from product api


def getProductFromAPI(product_id):
    base_api_url = app.config['PRODUCT_API_URL']
    response = requests.get(f'{base_api_url}{product_id}')
    if response.status_code == 200:
        product = json.loads(response.content)
        if product:
            return product
    else:
        return

# * Include one product to redis


def setProduct(product_dict, ttl):
    product_id = product_dict.pop('id')
    result = redis.hmset(product_id, product_dict)
    redis.expire(product_id, ttl)
    return result

# * Include all products from a page in product api to redis


def getOnePageAndSetToRedis(page, ttl):
    base_api_url = app.config['PRODUCT_API_URL']
    response = requests.get(f'{base_api_url}?page={page}')
    if response.status_code == 200:
        data = json.loads(response.content)
        info = data.get('meta')
        products = data.get('products')
        for product in products:
            setProduct(product, ttl)
        return info
    else:
        return []

# * Include all products from product api to redis


def getAllPages(ttl):
    result = getOnePageAndSetToRedis(1, ttl)
    while 'page_number' in result:
        result = getOnePageAndSetToRedis(result.get('page_number')+1, ttl)
    return {"total_products_in_redis": redis.dbsize()}

# *Remove one product from redis


def removeOne(key):
    result = redis.delete(key)
    return result


# * Clear redis DB


def clearRedis():
    redis.flushdb()
    return
