import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    PORT = os.environ.get('PORT') or 3000
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'SQLALCHEMY_DATABASE_URI') or'sqlite:///' + \
        os.path.join(basedir, 'database/db.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_SECRET_KEY = os.environ.get(
        'JWT_SECRET_KEY') or 'JustToHelpTestingDoNotUseInProduction'
    PRODUCT_API_URL = os.environ.get(
        'PRODUCT_API_URL') or "http://challenge-api.luizalabs.com/api/product/"
    REDIS_TTL = os.environ.get('REDIS_TTL') or 1800  # 30 minutes
