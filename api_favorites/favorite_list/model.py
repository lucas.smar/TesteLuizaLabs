from api_favorites import db, ma

# * FavoriteList Model


class FavoriteList(db.Model):
    client_id = db.Column(db.String(36), db.ForeignKey(
        'client.id'), nullable=False, primary_key=True)
    product_id = db.Column(db.String(36), nullable=False, primary_key=True)

    def __init__(self, client_id, product_id):
        self.client_id = client_id
        self.product_id = product_id

# * FavoriteList Schema


class FavoriteListSchema(ma.Schema):
    class Meta:
        fields = ('client_id', 'product_id')


favorite_schema = FavoriteListSchema()
favorite_list_schema = FavoriteListSchema(many=True)
