from flask import request, Blueprint, jsonify
from api_favorites import db
from api_favorites.favorite_list.model import FavoriteList, favorite_schema, favorite_list_schema
from api_favorites.auth.utils import token_required
from api_favorites.cache_products_api.utils import getProduct
import uuid
import requests
import json


favorite_list = Blueprint('favorite_list', __name__)

# * Apply token verify for all routes in client blueprint
@favorite_list.before_request
@token_required
def just_to_check_if_is_logged():
    pass


# * Add one product to a favorite list
@favorite_list.route('/client/<client_id>', methods=['POST'])
def include_product_to_client_list(client_id):
    product_id = request.json['product_id'] if 'product_id' in request.json else None
    if not product_id:
        return jsonify({"error": "Please send all required fields."})
    exists_in_client_list = FavoriteList.query.filter(
        FavoriteList.client_id == client_id, FavoriteList.product_id == product_id).first()
    if exists_in_client_list:
        return jsonify({'error': 'Product already in client favorite list.'})
    exists = getProduct(product_id)
    if not exists:
        return jsonify({"error": "This product id is not valid."})
    new_product_list = FavoriteList(client_id, product_id)
    db.session.add(new_product_list)
    db.session.commit()
    return favorite_schema.jsonify(new_product_list)

# * Get all products from the client favorite list
@favorite_list.route('/client/<client_id>', methods=['GET'])
def get_all_products_from_client_list(client_id):
    products = FavoriteList.query.filter_by(client_id=client_id)
    products = favorite_list_schema.dump(products)
    result = []
    for product in products:
        product_details = getProduct(product.get("product_id"))
        result.append(product_details)

    return jsonify(result)

# * Remove one product from the client favorite list
@favorite_list.route('/client/<client_id>', methods=['DELETE'])
def remove_product_from_client_list(client_id):
    product_id = request.json['product_id'] if 'product_id' in request.json else ''
    item = FavoriteList.query.filter(
        FavoriteList.client_id == client_id, FavoriteList.product_id == product_id).first()
    if item is None:
        return jsonify({'error': 'No product with this id in the client favorite list'})
    db.session.delete(item)
    db.session.commit()
    return favorite_schema.jsonify(item)
