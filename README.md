# Technical Challenge LuizaLabs

This api was developed as a possible solution to the technical challenge presented during the selection process for Backend developer.

The application was developed in Python3, using the microframework FLASK with a few plugins.

## Requirements

To manage the requirements was used [pipenv](https://pypi.org/project/pipenv/) along with Python 3.7, [Redis](https://redis.io/) and PosgreSQL (PosgreSQL is not necessary to test the api).

For the application to work, please check if you have these installed on your machine:

If you do not have pipenv installed run:

    pip install pipenv

In the project folder run the commands:

    pipenv shell
    pipenv install

Check if Redis is up and running on port 6379, run on your terminal:

     $redis-cli

You should see:

     127.0.0.1:6379>

Good it is up and running. Run to exit the redis-cli:

     127.0.0.1:6379>exit

Good! Now you have all dependencies that the application needs, and redis is running.

We are almost ready to run, it is just missing to check the api configuration variable (it is an optional step as all the configurations have a default value).

## Configuration Vars

If you want it is possible to change the configurations by environment variables:

### PORT

For the port that the api should run.

    export PORT=XXXX

By default 3000

### SQLALCHEMY_DATABASE_URI

For the uri of the database.

    export SQLALCHEMY_DATABASE_URI=""

For testing purposes a `sqlite` database was created and included in the repository and set as default. (! DO NOT USE IN PRODUCTION)

### JWT_SECRET_KEY

For the JWT signature.

    export JWT_SECRET_KEY=""

By default 'JustToHelpTestingDoNotUseInProduction' (! DO NOT USE THIS SECRET IN PRODUCTION)

### PRODUCT_API_URL

For the url of the api of products.

    export PRODUCT_API_URL="http://...."

By default 'http://challenge-api.luizalabs.com/api/product/'.

### REDIS_TTL

For the redis data retention time, in seconds.

    export REDIS_TTL=XX

By default 30 minutes (1800 seconds)

## Initializing new DB

Only if you change the default database it is necessary to initialize it. To do it run python on the terminal inside the project folder. Then run the following commands:

    from api_favorites import db, create_app
    db.create_all(app=create_app())
    quit()

## Running

To run the api run the command:

     python run.py

In the project folder. You should see something like this:

```
python run.py
* Serving Flask app "api_favorites" (lazy loading)
* Environment: development
* Debug mode: off
* Running on http://127.0.0.1:3000/ (Press CTRL+C to quit)
```

Good the api is running and ready to be used.

## Redis as cache

The idea is to reduce the amount of calls to the external api. As we can see in the diagram below, the external product api is only called if the product do not exist in Redis, and when this happens the return of the external api is saved in Redis, for future searches.

```mermaid
graph TB
    subgraph The use of Redis in the application
    A[Any call that has to get a product] -->B{Does the product exist in Redis?}
    B -->|YES| C[Return the product]
    B -->|NO| D[Call the External API]
    D -->E{Was the product found?}
    E -->|YES| F[Save in Redis]
    F --> C
    E -->|NO| G[Product do not exist at all]
    G --> H[Return null]
    end

```

## Using the api

The application has 4 main areas:

- Auth: used to login and include new applications that need access to the api.
- Client: used to create, update, view and delete clients.
- Favorite List: used to create, update, view and delete items from a client's favorite list.
- Cache Control: used to control the products cache from the external products api.

```mermaid
graph TB
    A["The api /"] --> B["/auth"]
    A --> C["/client"]
    A --> D["/list"]
    A --> E["/cache"]
```

### Secured Routes

For all the secured Routes, please include in the request header the token received after the success login.

    'Authorization': "Bearer TOKEN"

### Auth

#### Routes Diagram:

```mermaid
graph TB
    A["/auth"] --> B["/login [POST]"]
    A["/auth"] --> C["/newApp [POST]"]
```

#### Routes

##### /auth/login [POST]

This route receives a json as the model below:

    {
        "app_name":"the app name",
        "access_key": "the secret access key form the app"
    }

If the data is correct the api will send back a jwt token.

    {
        "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfbmFtZSI6InRlc3RlQXBwIiwiYWRtaW4iOnRydWUsImV4cCI6MTU3MjkwNDA2OH0.kE47zu351U9NK2ZGUXfTKOMA_kHf_yes5kidHl3NeXE"
    }

##### /auth/newApp [POST]

This route receives a json, as the model below:

    {
        "app_name":"new app name"

    }

If the data is correct the api will send back the new app access_key.

     {
         "access_key": "&zlJI#xlry.A0;h,dOuZ|(sY]p/|z&\*[y-kYC+zsBrA1wpo;XT",
         "message": "Please, keep this access key secure. It is your responsibility to remember it."
     }

### Client

#### Routes Diagram:

```mermaid
graph TB
    A["/client"] -->|Secured Routes| B["/ [POST]"]
    A["/client"] -->|Secured Routes| C["/ [GET]"]
    A["/client"] -->|Secured Routes| D["/:client_id [GET]"]
    A["/client"] -->|Secured Routes| E["/:client_id [PUT, PATCH]"]
    A["/client"] -->|Secured Routes| F["/:client_id [DELETE]"]
```

##### /client [POST] `Needs the authorization token`

This route receives a json, as the model below, and create a new client in db:

    {
        "name":"New client name",
        "email":"newClientEmail@mail.com"
    }

If the data is correct the api will send back the new client.

    {
        "id": "a872a955-8fa4-4124-a841-2dfd8c1ed905",
        "name":"New client name",
        "email":"newClientEmail@mail.com"
    }

##### /client [GET] `Needs the authorization token`

This route sends back all clients:

    [
        {
            "id": "cc396513-c0a0-4288-ba94-1c0a4e594836",
            "name":"New client name1",
            "email":"newClientEmail1@mail.com"
        },
        {
            "id": "a872a955-8fa4-4124-a841-2dfd8c1ed905",
            "name":"New client name2",
            "email":"newClientEmail2@mail.com"
        },
        .
        .
        .
    ]

##### /client/:client_id [GET] `Needs the authorization token`

This route sends back the selected client:

    {
        "id": "a872a955-8fa4-4124-a841-2dfd8c1ed905",
        "name":"SelectedClient",
        "email":"SelectedClientEmail@mail.com"
    }

##### /client/:client_id [PUT, PATCH] `Needs the authorization token`

This route receives a json, as the model below, and edit a selected client in db:

    {
        "name":"Client new name","email":"newClientEmail@mail.com"
    }

This route sends back the selected client:

    {
        "id": "a872a955-8fa4-4124-a841-2dfd8c1ed905",
        "name":"Client new name",
        "email":"newClientEmail@mail.com"
    }

##### /client/:client_id [DELETE] `Needs the authorization token`

This route delete a selected client and sends back the deleted client:

    {
        "id": "a872a955-8fa4-4124-a841-2dfd8c1ed905",
        "name":"Deleted client name",
        "email":"deletedClientEmail@mail.com"
    }

### Favorite List

#### Routes Diagram:

```mermaid
graph TB
    A["/list"] -->|Secured Routes| B["/client/:client_id [GET]"]
    A["/list"] -->|Secured Routes| C["/client/:client_id [POST]"]
    A["/list"] -->|Secured Routes| D["/client/:client_id [DELETE]"]
```

##### /list/client/:client_id [GET] `Needs the authorization token`

This route sends back all products on the selected client favorite list:

    [
        {
            "brand": "bébé confort",
            "image": "http://challenge-api.luizalabs.com/images/356eafd9-224a-d242-a3f2-e29b4270a927.jpg",
            "price": 1999.0,
            "title": "Cadeira para Auto Bébé Confort"
        },
        {
            "brand": "inovox",
            "image": "http://challenge-api.luizalabs.com/images/77be5ad3-fa87-d8a0-9433-5dbcc3152fac.jpg",
            "price": 160.69,
            "title": "Farol Lado Esquerdo para Monza 88/90"
        },
        .
        .
        .
    ]

##### /list/client/:client_id [POST] `Needs the authorization token`

This route receives a json, as the model below, and include a new product to the client favorite list:

    {
        "product_id":"813e2353-b98d-2dd2-ef5b-f033950aab33"
    }

If the data is correct the api will send back the new client.

    {
        "client_id": "theClientId",
        "product_id": "813e2353-b98d-2dd2-ef5b-f033950aab33"
    }

##### /list/client/:client_id [DELETE] `Needs the authorization token`

This route receives a json, as the model below, and delete a product from the client favorite list:

    {
        "product_id":"813e2353-b98d-2dd2-ef5b-f033950aab33"
    }

If the data is correct the api will send back the deleted item from the client list.

    {
        "client_id": "theClientId",
        "product_id": "813e2353-b98d-2dd2-ef5b-f033950aab33"
    }

### Cache Control

#### Routes Diagram:

```mermaid
graph TB
    A["/cache"] -->|Secured Routes| B["/seed [POST]"]
    A["/cache"] -->|Secured Routes| C["/cleanAll [POST, DELETE]"]
    A["/cache"] -->|Secured Routes| D["/addOrUpdate/product/:product_id [POST, PUT, PATCH]"]
    A["/cache"] -->|Secured Routes| E["/remove/product/:product_id [DELETE]"]
```

##### /cache/seed [POST] `Needs the authorization token`

This route receives a json, as the model below or nothing, try to seed the redis with all products from the external api:

    {
        "ttl":300 //OPTIONAL
    }

This task can take a long time to send back the response. Be patient.
If the redis is seed correct the api will send back the amount of products in redis:

    {
        "total_products_in_redis": 33304
    }

##### /cache/cleanAll [POST, DELETE] `Needs the authorization token`

This route deletes all products from redis and sends back the result:

    {
        "message": "Cache cleared!"
    }

##### /cache/addOrUpdate/product/:product_id [POST, PUT, PATCH] `Needs the authorization token`

This route receives a json, as the model below, and include or edit a product in redis cache:

    {
        "ttl":300 //OPTIONAL
    }

If the data is correct the api will send back the new client.

    {
        "message": "Product updated or add in cache!"
    }

##### /cache/remove/product/:product_id [DELETE] `Needs the authorization token`

This route delete one products from redis and sends back the result:

    {
        "message": "Product removed from cache!"
    }

## Author

Made with :heart: and rush by Lucas Martins.
